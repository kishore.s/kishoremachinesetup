

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class TestSetup {
    @Test
    public void shouldAssetNumber() {
        assertThat(1,is(equalTo(1)));
    }
}
